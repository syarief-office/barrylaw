/* Author:http://www.rainatspace.com

*/

function initializeScript(){
	//RESPONSIVE NAV
	jQuery('#menu').slicknav();

    //IMAGE FIT
	jQuery(".imgFill").imgLiquid({fill:true});
	jQuery(".imgNoFill").imgLiquid({fill:false});

	//WOW ANIMATE
	new WOW().init();
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
    
    //TOGGLE
	jQuery(".togle-nav").on("click", function(e){
		e.preventDefault();
		var link = jQuery(this);
		link.parent().parent().find('.togle-target').addClass('test').slideToggle('slow', function(){
			if(jQuery(this).is(":visible")){
				jQuery('.togle-nav span').replaceWith("<span>Less Detail <i class='fa fa-angle-up'></i></span>");	
			}else{
				jQuery('.togle-nav span').replaceWith("<span>More Detail <i class='fa fa-angle-down'></i></span>");		
			}
		});
	});

	//TABS
	jQuery("#tabs li").click(function() {
		jQuery("#tabs li").removeClass('active');
		jQuery(this).addClass("active");
		jQuery(".tab_content").hide();
		var selected_tab = $(this).find("a").attr("href");
		jQuery(selected_tab).fadeIn();
		return false;
	});
});
/* END ------------------------------------------------------- */



